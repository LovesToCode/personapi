/*
 * Copyright 2021 Steven Anthony (Tony) Williams.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.saw.spring.personApi.service;

import com.saw.spring.personApi.model.Person;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Steven Anthony (Tony) Williams
 */
public interface PersonService
{
   Person createPerson(Person person);
   Person updatePerson(Person person);
   Person updateFirstName(long id, String name);
   Person updateLastName(long id, String name);
   Person updateEMail(long id, String email);
   Person updatePhone(long id, String phone);
   Person getPersonById(long id);
   List<Person> getAllPeople();
   void deletePerson(long id);
   int count();
}
