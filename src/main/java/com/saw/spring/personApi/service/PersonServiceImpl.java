/*
 * Copyright 2021 Steven Anthony (Tony) Williams.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.saw.spring.personApi.service;

import com.saw.spring.personApi.ResourceNotFoundException;
import com.saw.spring.personApi.model.Person;
import com.saw.spring.personApi.model.repository.PersonRepo;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Steven Anthony (Tony) Williams
 */
@Service
@Transactional
public class PersonServiceImpl implements PersonService
{
   private enum UpdateFields { FirstName, LastName, Email, Phone }

   @Autowired
   private PersonRepo repo;

   @Override
   public List<Person> getAllPeople()
   {
      return repo.findAll();
   }

   @Override
   public Person createPerson(Person person)
   {
      return repo.save(person);
   }

   @Override
   public Person getPersonById(long id)
   {
      Optional<Person> dbPerson = repo.findById(id);

      if (dbPerson.isPresent())
         return dbPerson.get();
      else
         throw new ResourceNotFoundException("Read: Record not found with id: " + id);
   }

   @Override
   public Person updateFirstName(long id, String name)
   {
      return updateField(id, UpdateFields.FirstName, name);
   }

   @Override
   public Person updateLastName(long id, String name)
   {
      return updateField(id, UpdateFields.LastName, name);
   }

   @Override
   public Person updateEMail(long id, String email)
   {
      return updateField(id, UpdateFields.Email, email);
   }

   @Override
   public Person updatePhone(long id, String phone)
   {
      return updateField(id, UpdateFields.Phone, phone);
   }

   @Override
   public Person updatePerson(Person person)
   {
      Optional<Person> dbPerson = repo.findById(person.getId());

      if (dbPerson.isPresent())
      {
         String temp;
         Person personUpdate = dbPerson.get();

         //personUpdate.setId(person.getId()); // Redundant?
         temp = person.getFirstName();
         if (temp.length() > 0)
            personUpdate.setFirstName(temp);
         temp = person.getLastName();
         if (temp.length() > 0)
            personUpdate.setLastName(temp);
         temp = person.getEMail();
         if (temp.length() > 0)
            personUpdate.setEMail(temp);
         temp = person.getPhone();
         if (temp.length() > 0)
            personUpdate.setPhone(temp);
         personUpdate.setUserId(person.getUserId());

         return repo.save(personUpdate);
      }
      else
         throw new ResourceNotFoundException("Update: Record not found with id: " + person.getId());
   }

   @Override
   public void deletePerson(long id)
   {
      Optional<Person> dbPerson = repo.findById(id);

      if (dbPerson.isPresent())
         repo.delete(dbPerson.get());
      else
         throw new ResourceNotFoundException("Delete: Record not found with id: " + id);
   }

   @Override
   public int count()
   {
      return repo.findAll().size();
   }

   //////////////////
   // Private Methods

   private Person updateField(long id, UpdateFields field, String fieldValue)
   {
      Optional<Person> dbPerson = repo.findById(id);

      if (dbPerson.isPresent())
      {
         Person personUpdate = dbPerson.get();

         if (fieldValue.length() > 0)
         {
            switch (field)
            {
               case FirstName ->
               {
                  personUpdate.setFirstName(fieldValue);
               }
               case LastName ->
               {
                  personUpdate.setLastName(fieldValue);
               }
               case Email ->
               {
                  personUpdate.setEMail(fieldValue);
               }
               case Phone ->
               {
                  personUpdate.setPhone(fieldValue);
               }
            }
         }

         return repo.save(personUpdate);
      }
      else
         throw new ResourceNotFoundException("Update: Record not found with id: " + id);
   }
}
