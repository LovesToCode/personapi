/*
 * Copyright 2021 Steven Anthony (Tony) Williams.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.saw.spring.personApi.service;

import com.saw.spring.personApi.ResourceNotFoundException;
import com.saw.spring.personApi.model.Person;
import com.saw.spring.personApi.model.User;
import com.saw.spring.personApi.model.repository.PersonRepo;
import com.saw.spring.personApi.model.repository.UserRepo;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Steven Anthony (Tony) Williams
 */
@Service
@Transactional
public class UserServiceImpl implements UserService
{
   @Autowired
   private UserRepo repo;
   @Autowired
   private PersonRepo pRepo;

   @Override
   public User createUser(User user)
   {
      return repo.save(user);
   }

   @Override
   public User updateUser(User user)
   {
      Optional<User> dbUser = repo.findById(user.getId());

      if (dbUser.isPresent())
      {
         User userUpdate = dbUser.get();

         userUpdate.setId(user.getId()); // Possibly redundant
         userUpdate.setUserName(user.getUserName());
         userUpdate.setPassword(user.getPassword());
         userUpdate.setPersonId(user.getPersonId());

         return repo.save(userUpdate);
      }
      else
         throw new ResourceNotFoundException("Update: Record not found with id: " + user.getId());
   }

   @Override
   public User getUserById(long id)
   {
      Optional<User> dbUser = repo.findById(id);

      if (dbUser.isPresent())
         return dbUser.get();
      else
         throw new ResourceNotFoundException("Read: Record not found with id: " + id);
   }

   @Override
   public List<User> getAllUsers()
   {
      return repo.findAll();
   }

   @Override
   public void deleteUser(long id)
   {
      Optional<User> dbUser = repo.findById(id);

      if (dbUser.isPresent())
         repo.delete(dbUser.get());
      else
         throw new ResourceNotFoundException("Delete: Record not found with id: " + id);
   }

   @Override
   public int count()
   {
      return repo.findAll().size();
   }
}
