/*
 * Copyright 2021 Steven Anthony (Tony) Williams.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.saw.spring.personApi.controller;

import com.saw.spring.personApi.model.Person;
import com.saw.spring.personApi.service.PersonService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest controller for person data access.
 * @author Steven Anthony (Tony) Williams
 */
@RestController
public class PersonController
{
   public static final String GREETING = "Hello from people server.";

   @Autowired
   private PersonService personService;

   @GetMapping("/")
   public @ResponseBody String getGreeting()
   {
      return GREETING;
   }

   @GetMapping("/people")
   public @ResponseBody List<Person> getAllPeople()
   {
      return personService.getAllPeople();
   }

   @PostMapping("/people/create")
   public @ResponseBody Person createPerson(@RequestBody Person person)
   {
      return personService.createPerson(person);
   }

   @GetMapping("/people/read/{id}")
   public @ResponseBody Person getPersonById(@PathVariable long id)
   {
      return personService.getPersonById(id);
   }

   @PutMapping("/people/update/{id}/firstname")
   public @ResponseBody Person updateFirstName(@PathVariable long id, @RequestBody String name)
   {
      return personService.updateFirstName(id, name);
   }

   @PutMapping("/people/update/{id}/lastname")
   public @ResponseBody Person updateLastName(@PathVariable long id, @RequestBody String name)
   {
      return personService.updateLastName(id, name);
   }

   @PutMapping("/people/update/{id}/phone")
   public @ResponseBody Person updatePhone(@PathVariable long id, @RequestBody String phone)
   {
      return personService.updatePhone(id, phone);
   }

   @PutMapping("/people/update/{id}/email")
   public @ResponseBody Person updateEmail(@PathVariable long id, @RequestBody String email)
   {
      return personService.updateEMail(id, email);
   }

   @PutMapping("/people/update/{id}")
   public @ResponseBody Person updatePerson(@PathVariable long id, @RequestBody Person person)
   {
      person.setId(id);

      return personService.updatePerson(person);
   }

   @DeleteMapping("/people/delete/{id}")
   public HttpStatus deletePerson(@PathVariable long id)
   {
      personService.deletePerson(id);

      return HttpStatus.OK;
   }
}
