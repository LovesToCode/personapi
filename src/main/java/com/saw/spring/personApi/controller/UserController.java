/*
 * Copyright 2021 Steven Anthony (Tony) Williams.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.saw.spring.personApi.controller;

import com.saw.spring.personApi.model.User;
import com.saw.spring.personApi.service.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Steven Anthony (Tony) Williams
 */
@RestController
public class UserController
{
   @Autowired
   private UserService userService;

   @GetMapping("/user")
   public @ResponseBody List<User> getAllPeople()
   {
      return userService.getAllUsers();
   }

   @PostMapping("/user/create")
   public @ResponseBody User createUser(@RequestBody User user)
   {
      return userService.createUser(user);
   }

   @GetMapping("/user/read/{id}")
   public @ResponseBody User getUserById(@PathVariable long id)
   {
      return userService.getUserById(id);
   }

   @PutMapping("/user/update/{id}")
   public @ResponseBody User updateUser(@PathVariable long id, @RequestBody User user)
   {
      user.setId(id);

      return userService.updateUser(user);
   }

   @DeleteMapping("/user/delete/{id}")
   public HttpStatus deleteUser(@PathVariable long id)
   {
      userService.deleteUser(id);

      return HttpStatus.OK;
   }
}
