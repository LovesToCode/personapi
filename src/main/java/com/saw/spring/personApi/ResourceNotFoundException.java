/*
 * Copyright 2021 Steven Anthony (Tony) Williams.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.saw.spring.personApi;

import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception for database access.
 * @author Steven Anthony (Tony) Williams
 */
@ResponseStatus
public class ResourceNotFoundException extends RuntimeException
{
   public static final long serialVersionUID = 1L;

   public ResourceNotFoundException(String message)
   {
      super(message);
   }

   public ResourceNotFoundException(String message, Throwable throwable)
   {
      super(message, throwable);
   }
}
