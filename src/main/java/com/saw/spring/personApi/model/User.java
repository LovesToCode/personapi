/*
 * Copyright 2021 Steven Anthony (Tony) Williams.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.saw.spring.personApi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Data model entity for user info.
 * @author Steven Anthony (Tony) Williams
 */
@Entity
@Table(name = "user")
public class User
{
   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   @Column(name = "id", nullable = false)
   private long id;

   @Column(name = "userName", nullable = false)
   private String userName;

   @Column(name = "password", nullable = false)
   private String password;

   @Column(name = "personId", nullable = false)
   private long personId;

   public User() {}
   public User(String userName, String password)
   {
      this.userName = userName;
      this.password = password;
   }

   public long getId() { return id; }
   public void setId(long id) { this.id = id; }

   public String getUserName() { return userName; }
   public void setUserName(String userName) { this.userName = userName; }

   public String getPassword() { return password; }
   public void setPassword(String password) { this.password = password; }

   public long getPersonId() { return personId; }
   public void setPersonId(long personId) { this.personId = personId; }

   @Override
   public String toString()
   {
      return String.format("{User [Id: %d Username: %s Person Id: %d]}",
              id, userName, personId);
   }
}
