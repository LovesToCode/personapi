/*
 * Copyright 2021 Steven Anthony (Tony) Williams.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.saw.spring.personApi.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;

/**
 * Data model entity for person info.
 * @author Steven Anthony (Tony) Williams
 */
@Entity
@Table(name = "person")
public class Person
{
   private static final String EMPTY_FIELD = "";

   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   @Column(name = "id", nullable = false)
   private long id;

   @Column(name = "firstName", nullable = true)
   private String firstName;

   @Column(name = "lastName", nullable = true)
   private String lastName;

   @Column(name = "email", nullable = false)
   private String eMail;

   @Column(name = "phone", nullable = true)
   private String phone;

   @Column(name = "userId", nullable = true)
   private long userId;

   public Person() { }

   public Person(String firstName, String lastName,
                 String eMail, String phone)
   {
      this.firstName = firstName;
      this.lastName = lastName;
      this.eMail = eMail;
      this.phone = phone;
   }

   public long getId() { return id; }
   public void setId(long id) { this.id = id; }

   public String getFirstName() { return firstName; }
   public void setFirstName(String firstName) { this.firstName = firstName; }

   public String getLastName() { return lastName; }
   public void setLastName(String lastName) { this.lastName = lastName; }

   public String getEMail() { return eMail; }
   public void setEMail(String eMail) { this.eMail = eMail; }

   public String getPhone() { return phone; }
   public void setPhone(String phone) { this.phone = phone; }

   public long getUserId() { return userId; }
   public void setUserId(long userId) { this.userId = userId; }

   @Override
   public String toString()
   {
      return String.format("{Person [%s %s email: %s Phone: %s UserID: %d]}",
                           firstName, lastName, eMail, phone, userId);
   }
}
