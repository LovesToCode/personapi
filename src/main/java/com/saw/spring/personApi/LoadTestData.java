/*
 * Copyright 2021 Steven Anthony (Tony) Williams.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.saw.spring.personApi;

import com.saw.spring.personApi.model.Person;
import com.saw.spring.personApi.model.User;
import com.saw.spring.personApi.model.repository.PersonRepo;
import com.saw.spring.personApi.model.repository.UserRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * Loads test data into database upon startup.
 * @author Steven Anthony (Tony) Williams
 */
@Component
public class LoadTestData implements CommandLineRunner
{
   private static final Logger log = LoggerFactory.getLogger(LoadTestData.class);

   @Autowired
   private PersonRepo pRepo;

   @Autowired
   private UserRepo uRepo;

   @Override
   public void run(String... args) throws Exception
   {
      Person aPerson;
      User aUser;

      pRepo.deleteAll();
      uRepo.deleteAll();

      aPerson = new Person("Lazarus", "Long", "Senior@HowardFamily.net", null);
      aUser = new User("senior", "tanstaafl");
      aPerson.setUserId(uRepo.save(aUser).getId());
      pRepo.save(aPerson);

      aPerson = new Person("Rand", "AlThor", "Dragon@wot.net", "(800) 666-1111");
      aUser = new User("dragon", "sheepHearder1");
      aPerson.setUserId(uRepo.save(aUser).getId());
      pRepo.save(aPerson);

      aPerson = new Person("Frodo", "Baggins", "FBaggins@TheShire.net", "(801) 857-1122");
      aUser = new User("ringBearer", "nastyHobbit");
      aPerson.setUserId(uRepo.save(aUser).getId());
      pRepo.save(aPerson);

      aPerson = new Person("Daniel", "Olivaw", "IRobot@SteelCave.net", null);
      aUser = new User("iRobot", "The-3.Laws");
      aPerson.setUserId(uRepo.save(aUser).getId());
      pRepo.save(aPerson);
   }
}
