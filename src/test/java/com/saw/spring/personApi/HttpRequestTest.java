/*
 * Copyright 2021 Steven Anthony (Tony) Williams.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.saw.spring.personApi;

import com.saw.spring.personApi.controller.PersonController;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

/**
 * Tests the Http connection.
 * @author Steven Anthony (Tony) Williams
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class HttpRequestTest
{
   @LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
   public void serverShouldReturnDefaultGreeting()
   {
      assertThat(restTemplate.getForObject("http://localhost:" + port + "/", String.class))
              .contains(PersonController.GREETING);
   }

	@Test
   public void serverShouldReturnTestPerson()
   {
      String response = restTemplate.getForObject("http://localhost:" + port + "/people", String.class);

      assertThat(response).contains("Lazarus");
   }
}
