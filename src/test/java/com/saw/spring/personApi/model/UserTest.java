/*
 * Copyright 2021 Steven Anthony (Tony) Williams.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.saw.spring.personApi.model;

import com.saw.spring.personApi.Tests;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 *
 * @author Steven Anthony (Tony) Williams
 */
@SpringBootTest
public class UserTest
{
   @Test
   public void idTest()
   {
      User testUser = new User();

      testUser.setId(Tests.TEST_ID);
      assertThat(testUser.getId()).isEqualTo(Tests.TEST_ID);
   }

   @Test
   public void userNameTest()
   {
      User testUser = new User();

      testUser.setUserName(Tests.TEST_USER);
      assertThat(testUser.getUserName()).isEqualTo(Tests.TEST_USER);
   }

   @Test
   public void passwordTest()
   {
      User testUser = new User();

      testUser.setPassword(Tests.TEST_PASSWORD);
      assertThat(testUser.getPassword()).isEqualTo(Tests.TEST_PASSWORD);
   }
}
