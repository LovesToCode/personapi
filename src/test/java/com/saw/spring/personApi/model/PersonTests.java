/*
 * Copyright 2021 Steven Anthony (Tony) Williams.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.saw.spring.personApi.model;

import com.saw.spring.personApi.Tests;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 *
 * @author Steven Anthony (Tony) Williams
 */
@SpringBootTest
public class PersonTests
{
   @Test
   public void idTest()
   {
      Person testPerson = new Person();

      testPerson.setId(Tests.TEST_ID);
      assertThat(testPerson.getId()).isEqualTo(Tests.TEST_ID);
   }

   @Test
   public void firstNameTest()
   {
      Person testPerson = new Person();

      testPerson.setFirstName(Tests.TEST_FIRST_NAME);
      assertThat(testPerson.getFirstName()).isEqualTo(Tests.TEST_FIRST_NAME);
   }

   @Test
   public void lastNameTest()
   {
      Person testPerson = new Person();

      testPerson.setLastName(Tests.TEST_LAST_NAME);
      assertThat(testPerson.getLastName()).isEqualTo(Tests.TEST_LAST_NAME);
   }

   @Test
   public void emailTest()
   {
      Person testPerson = new Person();

      testPerson.setEMail(Tests.TEST_EMAIL);
      assertThat(testPerson.getEMail()).isEqualTo(Tests.TEST_EMAIL);
   }

   @Test
   public void phoneTest()
   {
      Person testPerson = new Person();

      testPerson.setPhone(Tests.TEST_PHONE);
      assertThat(testPerson.getPhone()).isEqualTo(Tests.TEST_PHONE);
   }
}
