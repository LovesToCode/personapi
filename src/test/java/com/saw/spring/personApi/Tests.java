/*
 * Copyright 2021 Steven Anthony (Tony) Williams.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.saw.spring.personApi;

/**
 *
 * @author Steven Anthony (Tony) Williams
 */
public class Tests
{
   public static final long TEST_ID = 1;
   public static final String TEST_FIRST_NAME = "Icabod";
   public static final String TEST_LAST_NAME = "Crane";
   public static final String TEST_EMAIL = "ICrane@SleepyHollow.net";
   public static final String TEST_PHONE = "(123) 456-7890";
   public static final String TEST_EMAIL_UPDATE = "Icabod1@SleepyHollow.net";

   public static final String TEST_PASS = "pass";
   public static final String TEST_FAIL = "fail";

   public static final String TEST_USER = "icabod";
   public static final String TEST_PASSWORD = "ghostHunter1";
}
